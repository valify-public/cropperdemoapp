//
//  ViewController.swift
//  ValifyDemo
//
//  Created by Sayed Obaid on 4/9/19.
//  Copyright © 2019 Valify. All rights reserved.
//

import Foundation
import UIKit
import ValifyCropperSDK
import Alamofire
class ViewController: UIViewController, valifyCropperDelegate, URLSessionDelegate, URLSessionDataDelegate {
    func cropperDidFinishProcess(_ valifyCropperResponse: valifyCropperResponse) {
        switch valifyCropperResponse {
        case .success(let cropperResult):
            print("Success")
            break
        case .error(let error):
            print(error.errorCode ?? -1)
            break
        case .userExited:
            print("user exited")
            break
        @unknown default:
            
            break
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            // Always adopt a light interface style.
            self.overrideUserInterfaceStyle = .light
        }
        
        valifyCropper.sharedInstance.delegate = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func KYC(_ sender: Any) {
        let bundle = ""

        let userName = ""

        let password = ""

        let clientID = ""

        let clientSecret = ""

        let baseURL = "https://www.valifystage.com/"
        let header = "\(baseURL)/api/o/token/"
        let head = HTTPHeaders.init(["Content-Type" :"application/x-www-form-urlencoded"])
        let parameters = [
            "username":userName,
                        "password":password,
                        "client_id":clientID,
                        "client_secret":clientSecret,
                        "grant_type":"password"
        ]
        let url = URL(string: header)!
        let arequest = AF.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: head)
        arequest.response { response in
            print(response)
            do {
                guard let jsonResponse = try JSONSerialization.jsonObject(with: (response.data!), options: JSONSerialization.ReadingOptions()) as? NSDictionary else {
                    return
                }
                print(jsonResponse)
                guard let token = jsonResponse.value(forKey: "access_token") as? String else {
                    return
                }
                let settings = valifyCropperBuilder()
                    .set(language: .english)
                    .captureFrontID(true)
                    .captureBackID(true)
                    .withoutCropping(false)
                    .forceLandscape(true)
                
                    .set(baseURL: baseURL)
                    .set(bundleKey: bundle)
                    .set(token: token)
                    .build()
                let valifyRun = valifyCropperFlow.init(withSettings: settings, fromViewController: self)
                do {
                    try valifyRun.run()
                } catch {
                    print(error)
                }
            }catch _ {
                print("not good JSON formatted response")
            }
        }

    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}
